#!/bin/bash

DS_IDS=$@
for folder in $DS_IDS
do
    echo "Entering $folder"
    echo "============================"
    cd /var/www/pylons-data/prod/PCAFiles/$folder
    folder_list=()
    cmd=''
    for i in $(ls -d */);
    do
        folder_list+=(${i%%/})

     done
    echo "Folders found - ${folder_list[@]}"
    echo "=============================================="

    #now iterate over all folders
    for i in "${folder_list[@]}"
        do
             echo "started folder --> $i"
             cmd+="INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (${folder} , 'ShowPCALinksOnDatasetSummaryPage' , '{\"name\":\"${i//_/ }\", \"url\":\"/datasets/pca?ds_id=${folder}&pca_type=${i}\"}' );"
             echo "finished folder --> $i"
             echo "======================"
        done
    echo ${cmd}
    psql -U portaladmin portal_prod -c " ${cmd}"
    echo "All done"
done
